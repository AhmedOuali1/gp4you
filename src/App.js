import React from 'react';
import './App.css';
import TekupList from "./tekup/TekupList"

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div className="tekup-team-title">
          <center>Tekup</center>
        </div>
        <center>NT-team</center>
        <TekupList />
      </header>
    </div>
  );
}

export default App;
